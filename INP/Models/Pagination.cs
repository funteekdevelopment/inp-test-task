﻿namespace INP.Models
{
    public class Pagination
    {
        public int total { get; set; }
        public int count { get; set; }
        public int per_page { get; set; }
        public int current_page { get; set; }
        public int total_pages { get; set; }
        public Link links { get; set; }
        public bool too_many { get; set; }

        public bool HasPreviousPage
        {
            get
            {
                return (current_page > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (current_page < total_pages);
            }
        }
    }
}
