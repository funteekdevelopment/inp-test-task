﻿using System.Collections.Generic;

namespace INP.Models
{
    public class ProductListView
    {
        public IEnumerable<DataProduct> data { get; set; }
        public PaginationInfo meta { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
    }
}
