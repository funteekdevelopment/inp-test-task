﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INP.Models
{
    public class ProductView
    {
        public string name { get; set; }
        public string type { get; set; }
        public string sku { get; set; }
        public string description { get; set; }
        public int weight { get; set; }
        public int width { get; set; }
        public int depth { get; set; }
        public int height { get; set; }
        public double price { get; set; }
        public double cost_price { get; set; }
        public double retail_price { get; set; }
        public int tax_class_id { get; set; }
        public string product_tax_code { get; set; }
        public List<int> categories { get; set; }
        public int brand_id { get; set; }
        public int inventory_level { get; set; }
        public int inventory_warning_level { get; set; }
        public string inventory_tracking { get; set; }
        public int fixed_cost_shipping_price { get; set; }
        public bool is_free_shipping { get; set; }
        public bool is_visible { get; set; }
        public bool is_featured { get; set; }
        public List<int> related_products { get; set; }
        public string bin_picking_number { get; set; }
        public string layout_file { get; set; }
        public string upc { get; set; }
        public string search_keywords { get; set; }
        public string availability { get; set; }
        public string gift_wrapping_options_type { get; set; }
        public List<object> gift_wrapping_options_list { get; set; }
        public int sort_order { get; set; }
        public string condition { get; set; }
        public bool is_condition_shown { get; set; }
        public int order_quantity_minimum { get; set; }
        public int order_quantity_maximum { get; set; }
        public List<object> meta_keywords { get; set; }
        public int view_count { get; set; }
        public DateTime? preorder_release_date { get; set; }
        public bool is_preorder_only { get; set; }
        public bool is_price_hidden { get; set; }
        public CustomUrl custom_url { get; set; }
        public string open_graph_type { get; set; }
    }
}
