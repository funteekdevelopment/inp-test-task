﻿using INP.Contracts.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INP.Contracts
{
    public class ShopApiUrls : IShopApiUrls
    {
        string store_hash = "ym5zwtl8bh";

        public string Products
        {
            get
            {
                return $"https://api.bigcommerce.com/stores/{store_hash}/v3/catalog/products";
            }
        }
    }
}
