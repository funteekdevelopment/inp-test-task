﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INP.Contracts.Interfaces
{
    public interface IShopApiUrls
    {
        string Products { get; }
    }
}
