﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using INP.Models;
using INP.Services.Contracts;

namespace INP.Controllers
{
    public class HomeController : Controller
    {
        private string store_hash = "ym5zwtl8bh";
        private IProductDataService _productDataService;

        public HomeController(IProductDataService productDataService)
        {
            _productDataService = productDataService;
        }

        public async Task<IActionResult> Index()
        {
            var products = await _productDataService.GetList($"https://api.bigcommerce.com/stores/{store_hash}/v3/catalog/products");

            return View(products);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
