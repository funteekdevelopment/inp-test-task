using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using INP.Services.Contracts;
using INP.Models;
using Newtonsoft.Json;
using System.Text;
using INP.Helpers;
using INP.Filters;

namespace INP.Controllers
{
    public class ProductController : Controller
    {
        //  store_hash = "ym5zwtl8bh"

        private IProductDataService _productDataService;

        const string _deleteSelectedUrl = "https://api.bigcommerce.com/stores/ym5zwtl8bh/v3/catalog/products?id:in={0}";
        const string _getUrl = "https://api.bigcommerce.com/stores/ym5zwtl8bh/v3/catalog/products";
        const string _createUrl = "https://api.bigcommerce.com/stores/ym5zwtl8bh/v3/catalog/products";
        const string _deleteUrl = "https://api.bigcommerce.com/stores/ym5zwtl8bh/v3/catalog/products/{0}";
        const string _detailsUrl = "https://api.bigcommerce.com/stores/ym5zwtl8bh/v3/catalog/products/{0}";

        public ProductController(IProductDataService productDataService)
        {
            _productDataService = productDataService;
        }

        #region Actions

        [HttpGet]
        public async Task<IActionResult> Index(string name, string sku, int page = 1)
        {
            var products = await _productDataService.GetList(_getUrl);

            products.data = products.data.FilterBy("name", name);
            products.data = products.data.FilterBy("sku", sku);
                        
            var viewModel = new ProductListView
            {
                data = products.data.Skip((page - 1) * PagiantionHelper.pageSize).Take(PagiantionHelper.pageSize),
                meta = PagiantionHelper.GetPagination(products, page),
                Name = name,
                Sku = sku
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(ProductListView products)
        {
            var removeIds = products.data.Where(item => item.remove).Select(item => item.id).ToArray();

            return RedirectToAction("Index");
        }

        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Create(ProductView product)
        {
            //// Stub product property
            product = DecorateProduct(product);
            /////


            if (ModelState.IsValid)
            {
                await _productDataService.Add(_createUrl, product);

                return RedirectToAction("Index");
            }

            return View(product);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteSelected(string[] ids, int page = 1)
        {
            var url = string.Format(_deleteSelectedUrl, ids.ToString(","));

            await _productDataService.Delete(url);

            return RedirectToAction("Index", new { page = page });
        }
        

        public async Task<IActionResult> Delete(string id, int page = 1)
        {
            var url = string.Format(_deleteUrl, id);

            await _productDataService.Delete(url);

            return RedirectToAction("Index", new { page = page });
        }

        public async Task<IActionResult> Details(string id)
        {
            var url = string.Format(_detailsUrl, id);

            var product = await _productDataService.Get(url);

            return View(product);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Create stub product
        /// </summary>
        /// <returns></returns>
        private ProductView DecorateProduct(ProductView product)
        {
            product.type = "physical";
            product.sku = new Random().Next(1, int.MaxValue).ToString();
            product.depth = 1;
            product.height = 1;
            product.price = 23;
            product.cost_price = 1;
            product.retail_price = 1;
            product.tax_class_id = 0;
            product.product_tax_code = "3451";
            product.brand_id = 0;
            product.inventory_level = 10;
            product.inventory_warning_level = 10;
            product.inventory_tracking = "none";
            product.fixed_cost_shipping_price = 8;
            product.is_free_shipping = false;
            product.is_visible = true;
            product.is_featured = true;
            product.related_products = new List<int> { -1 };
            product.bin_picking_number = "56WB";
            product.layout_file = "product.html";
            product.upc = "1234567890";
            product.search_keywords = "towel, bath, kitchen";
            product.availability = "available";
            product.gift_wrapping_options_type = "any";
            product.gift_wrapping_options_list = new List<object>();
            product.sort_order = 4;
            product.condition = "New";
            product.is_condition_shown = true;
            product.order_quantity_minimum = 5;
            product.order_quantity_maximum = 50;
            product.meta_keywords = new List<object>();
            product.view_count = 146;
            product.preorder_release_date = DateTime.Parse("2005-12-30T01:02:03+00:00");
            product.is_preorder_only = false;
            product.is_price_hidden = false;
            product.custom_url = new CustomUrl
            {
                is_customized = true,
                url = "/this-is-a-custom-url/" + new Random().Next(1, int.MaxValue).ToString()
            };
            product.open_graph_type = "product";
            product.categories = new List<int> { 23, 18 };

            return product;
        }

        #endregion
    }
}