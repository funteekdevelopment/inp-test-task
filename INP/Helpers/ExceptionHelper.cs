﻿using INP.Exceptions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INP.Helpers
{
    public static class ExceptionHelper
    {
        public static ContentResult CreateError(Exception exception)
        {
            ContentResult error;
            switch (exception.GetType().Name)
            {
                case nameof(DuplicateProductException):
                    error = new ContentResult {  Content = "The product is a duplicate" };
                    break;
                case nameof(NotValidProductException):
                    error = new ContentResult { Content = "Product was not valid. This is the result of missing required fields, or of invalid data. See the response for more details." };
                    break;
                default:
                    error = new ContentResult
                    {
                        Content = "An error has occured"
                    };
                    break;
            }

            return error;
        }
    }
}
