﻿using INP.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace INP.Helpers
{
    public static class ValidateHelper
    {
        public static void StatusCode(HttpStatusCode statusCode)
        {
            switch (statusCode)
            {
                case (HttpStatusCode.Conflict):
                    throw new DuplicateProductException();
                case ((HttpStatusCode)422):
                    throw new NotValidProductException();
                case (HttpStatusCode.InternalServerError):
                    throw new Exception();
                default:
                    break;
            }
        }
    }
}
