﻿using INP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INP.Helpers
{
    public class PagiantionHelper
    {
        public static int pageSize = 10;

        public static PaginationInfo GetPagination(ProductListView productList, int page)
        {
            productList.meta.pagination.per_page = pageSize;

            productList.meta.pagination.current_page = page;

            productList.meta.pagination.total_pages = productList.data.Count() / pageSize; 

            if (productList.data.Count() % pageSize != 0) productList.meta.pagination.total_pages++;

            return productList.meta;
        }
    }
}
