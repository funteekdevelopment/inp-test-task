﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace INP.Helpers
{
    public static class AuthorizedRequestHelper
    {
        public static HttpRequestMessage PrepareHttpRequest(string uri, HttpMethod httpMethod)
        {
            const string token = "eyf1w7j3q7zgh8ww0g6uthj2nxw5db2";
            const string clientId = "r1c06imzfjs86z4k8jpm5ehr1xqafq5";

            var request = new HttpRequestMessage(httpMethod, uri);
            
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("X-Auth-Client", clientId);
            request.Headers.Add("X-Auth-Token", token);

            return request;
        }
    }
}
