﻿using INP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INP.Helpers
{
    public static class FilterHelper
    {
        public static IEnumerable<DataProduct> FilterBy(this IEnumerable<DataProduct> products, string filter, string valueFilter)
        {
            if(valueFilter != null)
            {
                switch (filter)
                {
                    case "name":
                        products = products.Where(item => item.name.ToUpper().Contains(valueFilter.ToUpper())).ToList();
                        break;
                    case "sku":
                        products = products.Where(item => item.sku.ToUpper().Contains(valueFilter.ToUpper())).ToList();
                        break;
                    default:
                        break;
                }
            }
            
            return products;
        }
    }
}
