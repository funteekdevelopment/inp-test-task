﻿using System.Collections.Generic;
using INP.Services.Contracts;
using INP.Helpers;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using System;
using INP.Exceptions;

namespace INP.Services
{
    public class DataService<TEntityCreate, TEntityGet, TEntityList> : IDataService<TEntityCreate, TEntityGet, TEntityList> 
        where TEntityCreate : class
        where TEntityGet : class
        where TEntityList : class
    {
        private static readonly HttpClient _client = new HttpClient();

        public async Task<TEntityList> GetList(string url)
        {
            var request = AuthorizedRequestHelper.PrepareHttpRequest(url, HttpMethod.Get);
            var response = await _client.SendAsync(request);
            ValidateHelper.StatusCode(response.StatusCode);
            var resultStr = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<TEntityList>(resultStr);
            return result;
        }

        public async Task<TEntityGet> Get(string url)
        {
            var request = AuthorizedRequestHelper.PrepareHttpRequest(url, HttpMethod.Get);
            var response = await _client.SendAsync(request);
            ValidateHelper.StatusCode(response.StatusCode);
            var resultStr = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<TEntityGet>(resultStr);
            return result;
        }

        public async Task Add(string url, TEntityCreate entity)
        {
            var request = AuthorizedRequestHelper.PrepareHttpRequest(url, HttpMethod.Post);
            request.Content = new StringContent(JsonConvert.SerializeObject(entity));
            var response = await _client.SendAsync(request);
            ValidateHelper.StatusCode(response.StatusCode);
        }

        public async Task Delete(string url)
        {
            var request = AuthorizedRequestHelper.PrepareHttpRequest(url, HttpMethod.Delete);
            var response = await _client.SendAsync(request);
            ValidateHelper.StatusCode(response.StatusCode);
        }
    }
}
