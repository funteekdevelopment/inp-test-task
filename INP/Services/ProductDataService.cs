﻿using INP.Models;
using INP.Services.Contracts;

namespace INP.Services
{
    public class ProductDataService : DataService<ProductView, ProductGetView, ProductListView>, IProductDataService
    {
        
    }
}
