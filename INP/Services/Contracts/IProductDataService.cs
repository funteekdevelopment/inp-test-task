﻿using INP.Models;

namespace INP.Services.Contracts
{
    public interface IProductDataService : IDataService<ProductView, ProductGetView, ProductListView>
    {
    }
}
