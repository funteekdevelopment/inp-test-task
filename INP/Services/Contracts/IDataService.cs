﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INP.Services.Contracts
{
    public interface IDataService<TEntityCreate, TEntityGet, TEntityList> 
        where TEntityCreate : class
        where TEntityGet : class
        where TEntityList : class
    {
        Task<TEntityList> GetList(string url);
        Task<TEntityGet> Get(string url);
        Task Add(string url, TEntityCreate entity);
        Task Delete(string url);
    }
}
