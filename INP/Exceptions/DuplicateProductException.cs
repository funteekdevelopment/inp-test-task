﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INP.Exceptions
{
    public class DuplicateProductException : Exception
    {
        public DuplicateProductException()
        {
        }
    }

    public class NotValidProductException : Exception
    {
        public NotValidProductException()
        {
        }
    }
}
